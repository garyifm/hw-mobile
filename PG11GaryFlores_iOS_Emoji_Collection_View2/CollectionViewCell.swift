//
//  CollectionViewCell.swift
//  PG11GaryFlores_iOS_Emoji_Collection_View2
//
//  Created by Gary Flores on 3/11/18.
//  Copyright © 2018 GaryIfm. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var myEmojiLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func changeLabel(newLabel: String ){
        myEmojiLabel.text = newLabel
    }

}

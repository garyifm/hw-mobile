//
//  ViewController.swift
//  PG11GaryFlores_iOS_Emoji_Collection_View2
//
//  Created by Gary Flores on 3/11/18.
//  Copyright © 2018 GaryIfm. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var myColView: UICollectionView!
    var myEmojiAr :[String] = ["😃", "😅", "😡"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let nib = UINib(nibName: "CollectionViewCell", bundle: nil)
        myColView.register(nib, forCellWithReuseIdentifier: "CustomCollectionViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myEmojiAr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentEmoji = myEmojiAr[indexPath.row]
        let myCell = myColView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as! CollectionViewCell
        myCell.changeLabel(newLabel: currentEmoji)
        return myCell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

